"""Downloads words from Marugoto Web API and create flash cards in Anki Web."""
import configparser
import urllib
import time
from robobrowser import RoboBrowser
import requests

CONFIG_PATH = './application.cfg'

MARUGOTO_URL_FORMAT = 'http://words.marugotoweb.jp/SearchCategoryAPI?&lv={}&tp={}&tx=act,comp,vocab&ut=en'
ANKI_LOGIN_URL = 'https://ankiweb.net/account/login'
ANKI_URL = 'https://ankiweb.net/edit/save'


def __run():
    print('Initializing...')
    config = __load_config()
    flash_cards = __get_flash_cards(config)
    __save_flash_cards(flash_cards, config)
    print('All done :^)')


def __load_config():
    print('Loading configuration...')
    config_parser = configparser.RawConfigParser()
    config_parser.read(CONFIG_PATH)

    # Make sure configuration file is valid
    config_parser.get('Marugoto', 'LEVEL')
    config_parser.get('Marugoto', 'TOPIC')
    config_parser.get('Anki', 'USERNAME')
    config_parser.get('Anki', 'PASSWORD')
    config_parser.get('Anki', 'CSRF_TOKEN')
    config_parser.get('Anki', 'DECK_ID')
    config_parser.get('Anki', 'DECK_NAME')

    return config_parser


def __get_flash_cards(config: configparser.RawConfigParser):
    print('Downloading vocabulary for level {} and topic {}...'
          .format(config.get('Marugoto', 'LEVEL'), config.get('Marugoto', 'TOPIC')))

    url = MARUGOTO_URL_FORMAT.format(config.get('Marugoto', 'LEVEL'), config.get('Marugoto', 'TOPIC'))
    response = requests.get(url)
    response.raise_for_status()

    response_data = response.json()
    print('Found {} words.'.format(response_data['HIT']))

    return [
        {
            'KANA': d['KANA'],
            'ROMAJI': d['ROMAJI'],
            'UWRD': d['UWRD'].replace('\ufeff', '').replace('"', '')
        }
        for d in response_data['DATA']
    ]


def __save_flash_cards(flash_cards: [], config: configparser.RawConfigParser):
    print('Saving cards to deck {}...'.format(config.get('Anki', 'DECK_NAME')))
    browser = RoboBrowser(history=True, parser='html.parser')
    browser.open('https://ankiweb.net/account/login')
    login_form = browser.get_forms()[0]
    login_form['username'] = config.get('Anki', 'USERNAME')
    login_form['password'] = config.get('Anki', 'PASSWORD')
    browser.submit_form(login_form)

    # TODO: We probably don't need to set absolutely all of these headers
    browser.session.headers.update({'accept': '*/*'})
    browser.session.headers.update({'accept-encoding': 'gzip, deflate, br'})
    browser.session.headers.update({'accept-language': 'en-US,en;q=0.8'})
    browser.session.headers.update({'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'})
    browser.session.headers.update({'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.91 Safari/537.36'})
    browser.session.headers.update({'dnt': '1'})
    browser.session.headers.update({'origin': 'https://ankiweb.net'})
    browser.session.headers.update({'referer': 'https://ankiweb.net/edit/'})

    vocabulary_length = len(flash_cards)
    for (index, word) in enumerate(flash_cards):
        uploaded_word_count = index + 1
        print('[{}/{}] Saving {}...'.format(uploaded_word_count, vocabulary_length, word))
        # Don't even ask
        data = 'data=%5B%5B%22{}%22%2C%22{}%22%5D%2C%22%22%5D&csrf_token={}&mid={}&deck={}'.format(
            urllib.parse.quote(word['KANA']),
            urllib.parse.quote(word['UWRD']),
            config.get('Anki', 'CSRF_TOKEN'),
            config.get('Anki', 'DECK_ID'),
            urllib.parse.quote(config.get('Anki', 'DECK_NAME')))
        response = browser.session.post(ANKI_URL, data=data)
        response.raise_for_status()
        # Sleep for a minute every 30 uploads so we don't get an HTTP 429 :^)
        if uploaded_word_count % 30 == 0:
            print('Sleeping...')
            time.sleep(60)

if __name__ == '__main__':
    __run()
