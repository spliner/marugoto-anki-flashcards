# Marugoto -> Anki Flashcard Generator

## Configuration
Create a file called application.cfg like the example below:
```
[Marugoto]
LEVEL = A1
TOPIC = 1

[Anki]
USERNAME = anki_username
PASSWORD = anki_password
DECK_NAME = anki_deck_name
DECK_ID = anki_deck_id
CSRF_TOKEN = anki_csrf_token
```
